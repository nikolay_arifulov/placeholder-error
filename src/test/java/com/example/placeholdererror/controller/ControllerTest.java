package com.example.placeholdererror.controller;

import lombok.SneakyThrows;

import org.junit.Before;
import org.junit.Test;

import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = Controller.class)
public class ControllerTest {

    private MockMvc mockMvc;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders
                .standaloneSetup(new Controller())
                .build();
    }

    @Test
    @SneakyThrows
    public void test1() {
        mockMvc.perform(post("/test1"))
                .andExpect(status().isOk());
    }

    @Test
    @SneakyThrows
    public void test2() {
        mockMvc.perform(post("/test2"))
                .andExpect(status().isOk());
    }
}
