package com.example.placeholdererror.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

    @PostMapping(value = "/test1")
    public void test1() {
        System.out.println("TEST1");
    }

    @PostMapping(value = "${url}")
    public void test2() {
        System.out.println("TEST2");
    }
}
