package com.example.placeholdererror;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PlaceholderErrorApplication {

    public static void main(String[] args) {
        SpringApplication.run(PlaceholderErrorApplication.class, args);
    }

}
